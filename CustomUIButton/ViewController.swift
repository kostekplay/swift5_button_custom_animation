////  ViewController.swift
//  CustomUIButton
//
//  Created on 03/06/2020.
//  Copyright © 2020 kostowski. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var customButton = CustomButton(frame: CGRect(x: 0, y: 0, width: 200, height: 50))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customButton.setTitle("Button 2", for: .normal)
        
        addActionToButton()
        
        customButton.center = view.center
        
        view.addSubview(customButton)
    }

    
    func addActionToButton() {
        customButton.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
    }
    
    @objc func buttonTapped() {
        customButton.shakeButton()
    }
}

